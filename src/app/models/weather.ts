export interface Weather {
    min_temperature: number,
    max_temperature: number,
    temperature_unit: string,
    weatherCode: number,
    sunrise: string,
    sunset: string,
    precipitation: number,
    precipitation_unit: string,
    time: string
}
