import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, Observable } from 'rxjs';
import { City } from 'src/app/models/city';
import { Weather } from 'src/app/models/weather';
import { TimePipe } from 'src/app/pipes/time/time.pipe';
import { OpenMeteoApiService } from 'src/app/services/open-meteo-api/open-meteo-api.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  cities: City[] = [
    {
      name: "Warsaw",
      latitude: 52.23,
      longitude: 21.01
    },
    {
      name: "Prague",
      latitude: 50.09,
      longitude: 14.42
    }
  ]

  currentCity: City = this.cities[0];

  previousDays: number[] = [...Array(15).keys()];

  selectedPreviousDays: number = 0;

  panelColor = new FormControl(this.cities[0]);

  $weather: Observable<any> | undefined;

  constructor(private meteoService: OpenMeteoApiService, private timePipe: TimePipe) { }

  ngOnInit(): void {
    this.getWeather();
  }

  getWeather(): void {
    this.$weather = this.meteoService.getTodayWeather(this.currentCity, this.selectedPreviousDays).pipe(map((res: any) => {
      let weather: Weather[] = []
      res.daily.time.forEach((el: any, index: number) => {
        weather.push({
          min_temperature: res.daily.temperature_2m_min[index],
          max_temperature: res.daily.temperature_2m_max[index],
          temperature_unit: res.daily_units.temperature_2m_max,
          weatherCode: res.daily.weathercode[index],
          sunrise: this.timePipe.transform(res.daily.sunrise[index]),
          sunset: this.timePipe.transform(res.daily.sunset[index]),
          precipitation: res.daily.precipitation_sum[index],
          precipitation_unit: res.daily_units.precipitation_sum,
          time: el
        })
      })

      return weather
    }
    ))
  }


}
