import { Component, Input } from '@angular/core';
import { City } from 'src/app/models/city';
import { Weather } from 'src/app/models/weather';
import { WmoCodesService } from 'src/app/services/wmoCodes/wmo-codes.service';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss']
})
export class WeatherCardComponent {

  @Input() city: City | undefined;
  @Input() weather: Weather | undefined;

  constructor(private wmoCodesService: WmoCodesService) {

  }

  decodeWeather(code: number | undefined): { codes: number[]; label: string; icon: string; } | undefined {
    return this.wmoCodesService.dedcode(code)
  }
}
