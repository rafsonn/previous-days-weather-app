import { formatDate } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date'
})
export class DatePipe implements PipeTransform {

  transform(value: string | undefined): string {
    return value ? formatDate(value, 'dd.MM', 'en') : '';
  }

}
