import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { City } from '../../models/city';

@Injectable({
  providedIn: 'root'
})
export class OpenMeteoApiService {

  constructor(private http: HttpClient) { }

  getTodayWeather(city: City, pastDays: number = 0) {
    return this.http.get(`https://api.open-meteo.com/v1/forecast?latitude=${city.latitude}&longitude=${city.longitude}&daily=weathercode,sunrise,sunset,precipitation_sum,temperature_2m_max,temperature_2m_min&past_days=${pastDays}&forecast_days=1&timezone=Europe%2FLondon`)
  }
}