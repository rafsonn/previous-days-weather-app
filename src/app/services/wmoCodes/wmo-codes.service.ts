import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WmoCodesService {

  constructor() { }

  private codes = [
    { codes: [0], label: 'Clear Sky', icon: 'bi-brightness-high' },
    { codes: [1, 2, 3], label: 'Partly Cloudy', icon: 'bi-cloudy' },
    { codes: [45, 48], label: 'Fog', icon: 'bi-cloud-fog' },
    { codes: [51, 53, 55, 56, 57], label: 'Drizzle', icon: 'bi-cloud-drizzle' },
    { codes: [61, 63, 65, 66, 67, 80, 81, 82], label: 'Rain', icon: 'bi-cloud-rain' },
    { codes: [71, 73, 75, 77, 85, 86], label: 'Snow', icon: 'bi-snow' },
    { codes: [95, 96, 99], label: 'Thunderstorm', icon: 'bi-cloud-lightning' },
  ]

  dedcode(code: number | undefined) {
    return code ? this.codes.find(el => el.codes.find(e => e === code)) : undefined
  }

}
