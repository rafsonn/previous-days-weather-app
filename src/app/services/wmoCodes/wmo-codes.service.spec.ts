import { TestBed } from '@angular/core/testing';

import { WmoCodesService } from './wmo-codes.service';

describe('WmoCodesService', () => {
  let service: WmoCodesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WmoCodesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
