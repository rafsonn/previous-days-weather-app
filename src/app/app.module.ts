import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { WeatherComponent } from './components/weather/weather.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TimePipe } from './pipes/time/time.pipe';
import { DatePipe } from './pipes/date/date.pipe';
import { WeatherCardComponent } from './components/weather-card/weather-card.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    TimePipe,
    DatePipe,
    WeatherCardComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    MatSelectModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [TimePipe, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
